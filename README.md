# Readme

Author: Dennie te Molder
Date: 8 Apr 2021
Contact: Jasper Koehorst (jasper.koehorst@wur.nl)

## File Overview

* **SQL_Pathogenicity_Database**
  * *clear_database.sql* - Empty the existing database
  * *create_database.sql* - Create the empty pathogenicity database
  * *my_queries.sql* - Queries used to determine (non)-pathogens and create database summaries
  * *pathogenicity_database_backup_13-6-2021.sql* - Backup of the filled *Xanthomonas* SQL pathogenicity database
  * *xanthomonas.mwb* - Schema (MySQL workbench) of the pathogenicity database 
  * *xanthomonas.bak* - Schema (MySQL workbench) of the pathogenicity database 
* **RCode**
  * **Data**
    * *assembly_summary_genbank.zip* - Snapshot of genbank accessions used for genome retrieval
    * *cart.Rdata* - R list containing the CART results from the 20x5-4x10 CV loop 
    * *domain_descriptions.csv* - Descriptions of the Pfam domains found in the retrieved genomes
    * *domain_matrix.csv* - Binary presence/absence matrix  of pfam domain (columns) in the *Xanthomonas* genoms (rows)
    * *genome_information.csv* - Metadata of the retrieved *Xanthomonas* genomes including their pathogenicity
    * *lasso.Rdata* - R list containing the Lasso results from the 20x5-4x10 CV loop 
    * *pancore.Rdata* - R data frame containing observed/estimated pan and core domainome for several sample sizes 
    * *pathogenicity_database.xlsx* - Input form for the pathogenicity database
    * *proteins_with_domains.tar.gz* - Compressed file containing the sequences of proteins containing the domain of the file description
    * *random_forest.Rdata* - R list containing the Random Forest results from the 20x5-4x10 CV loop 
  * **MyFunctions** - Library of custom function used for model training and testing
  * *01_input_data.R* - Processes and inputs all of the tests and papers from "Data/pathogenicity_database.xlsx" into the SQL pathogenicity databse
  * *02_genbank.R* - Queries the pathogenicity database to determine (non-)pathogenic strains and checks which of these strains have a genome listed in "Data/assembly_summary_genbank.zip" and proceeds to download these genomes
  * *03_annotate.sh* - Bash shell commands used to annotate the retrieved genomes using SAPP
  * *04_sparql.R* - Interfaces with the GraphQL database that holds the annotation results to obtain statistics and create the binary domain presence/absence matrix.
  * *05_data_analysis.R* - Exploration of the created metadata and domain matrix
  * *06_model_building.R* - Model building and visualization of results
  * *07_check_individual_domain.R* - Auxiliary script that check presence and enrichment of a single domain and writes the sequences of proteins that contain the selected domain to a file
  * *MyFunctions_0.0.0.9000.tar.gz* - MyFunctions source package required for install
  * *renv.lock* - File containing all R packages and version used for analysis, used by [Renv](https://rstudio.github.io/renv/articles/renv.html)
  * *Xanthomonas_paper.Rproj* - Rstudio project file required for Renv to work

# Analysis

## Step 1: Create and fill the *Xanthomonas* pathogenicity database

The *Xanthomonas* pathogenicity database was designed as a relational database (SQL). To recreate the database the [MariaDB](https://mariadb.org/) SQL server needs to be installed on the local machine.  [MySQL Workbench](https://www.mysql.com/fr/products/workbench/) was used as a front-end interface for database management. Within MariaDB a database called "xanthomonas" should be creaed on "localhost:3306". Within the database a user called "xanthomonas" with password "xanthomonas" should be created that has permission to create views and run queries. After this there are two different options to create an fill the pathogenicity database:

```
# SQL Code to create user just to make life easy (something like this?)
CREATE USER xanthomonas identified by "xanthomonas";
```

### Option 1: Using the backup file

1. Import the "SQL_Pathogenicity_Database/*pathogenicity_database_backup_13-6-2021.sql*" file through MySQL Workbench (Server > Data Import > Import from Self-Contained File) with as default schema an empty 'xanthomonas' schema.

### Option 2: From an empty database

*This is useful when you have added additional content to the excel file.*

1. Run the "Pathogenicity_Database/created_database.sql" script trough MySQL Workbench whilst connected to the "xanthomonas" database to create the emtpy database.
2. Run the "01_input_data.R" R script to parse the "Data/pathogenicity_database.xlsx" file and load this data into the empty database.

## Step 2: Query the database and retrieve the matching genomes

Run the "Code/02_genbank.R" R script. This script connects to the pathogenicity database to retrieve a list of pathogenic and non-pathogenic *Xanthomonas* strains. The names and aliases of these strains are then compared to the entries in "Data/assembly_summary_genbank.zip" to obtain the matching genomes. These genomes are downloaded from the NCBI ftp server and stored in the "genomes" folder. The metadata of the strains for which a genome is found is stored in "Code/Data/genome_information.csv".

## Step 3: Annotate the retrieved genomes using SAPP

Unzip all of the genomes in the "genomes" folder. Download the *Conversion.jar*, *genecaller.jar*, and *InterProScan.jar* models from the [SAPP download page](http://download.systemsbiology.nl/sapp/) and place these files in the "genomes/sapp" folder. On a linux machine run the "Code/03_annotate.sh" from the "genomes" folder to annotate the genomes. Make sure the machine has Java Runtime Environment and GNU parallel installed.

## Step 4: Query the annotation information

Download and install [GraphDB Free](https://www.ontotext.com/products/graphdb/graphdb-free/). Create a repository called "genomes" using the default settings and import all of the annotated "genomes/*.ttl" files into the database. Run the "Code/04_sparql.R" R script to extract the relevant annotation information form the GraphDB annotation database. This script stores the binary domain presence/absence matrix in "Code/Data/domain_matrix.csv" and appends the annotation statistics to "Code/Data/genome_information.csv".

## Step 5: Data Exploration & Model Building

The "Code/05_data_analysis.R" R script summarizes and visualizes the annotation statistics, creates a PCA from the domain matrix, and runs the pan/core analysis. The results from this pan/core analysis can be found in "Code/Data/pancore.Rdata". The "Code/06_model_building.R" R script calculates the enrichments of the different domains in pathogens vs. non-pathogens, filters the domain matrix, visualizes this matrix using a PLS, trains and tests the different models on this filtered matrix, visualizes the resulting performance statistics, and extracts the relevant predictors (domains) from the created models. The created models can be found in "Code/Data/" folder as "cart.Rdata", "lasso.Rdata", and "random_forest.Rdata". The "Code/07_check_individual_domain" script can be used to explore the enrichment of single domains over the different species and to retrieve the sequences of proteins that contain the selected domain. The resulting fasta files containing all proteins with the selected domain (Fasta files for the most important domains can be found in "Code/Data/proteins_with_domains.tar.gz") were submitted to [Muscle](https://www.ebi.ac.uk/Tools/msa/muscle/) for global alignment. The resulting tree was examined and the protein sequences representative of the different clusters were submitted to [BLASTp](https://blast.ncbi.nlm.nih.gov/Blast.cgi?PROGRAM=blastp&PAGE_TYPE=BlastSearch&LINK_LOC=blasthome) to determine their identity.