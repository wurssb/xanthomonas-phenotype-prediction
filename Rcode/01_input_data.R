# Setup -------------------------------------------------------------------

# Renv uses the "renv.lock" file to install identical package versions
install <- F
if (install) renv::restore()

library(tidyverse)

# Open DB connection
library(DBI)
connection <- dbConnect(
  drv = RMariaDB::MariaDB(),
  dbname = "xanthomonas",
  username = "xanthomonas",
  password = "xanthomonas",
  host = "127.0.0.1",
  port = 3306,
)

# Choices
pathogenicityOptions <- c("non-pathogenic", "pathogenic")
inoculationOptions <- c("dip", "clipping", "puncture", "incision", "infiltration",
                        "spray", "curation", "submission", "unknown")
symptomsOptions <- c("none", "mild", "normal", "severe", "unspecified")


# Functions ---------------------------------------------------------------

insertData <- function(tibble, table) {
  columns <- str_c(colnames(tibble), collapse = ", ")

  for(i in 1:nrow(tibble)) {
    values <- str_c(tibble[i,], collapse = "', '")

    sql <- str_interp("
      INSERT INTO ${table} (${columns})
      VALUES ('${values}')")
    dbExecute(connection, sql)
  }
}

matchKey <- function(tibble, table) {
  if (!nrow(tibble) == 1) stop("Input tibble can only have one row!")

  # Generate criteria for the WHERE clause and build query
  criteria <- str_c(colnames(tibble), tibble, sep = " = '", collapse = "' AND ")
  sql <- str_interp("
    SELECT * FROM ${table}
    WHERE ${criteria}'
  ")

  # Send query
  result <- dbSendQuery(connection, sql)
  data <- dbFetch(result)
  dbClearResult(result)

  # Check length of match
  if (nrow(data) > 1) {
    stop("Match not unique!")
  } else if (nrow(data) == 1) {
    return(TRUE)
  } else {
    return(FALSE)
  }
}

fetchData <- function(tibble, table) {
  if (!nrow(tibble) == 1) stop("Input tibble can only have one row!")

  # Generate criteria for the WHERE clause and build query
  criteria <- str_c(colnames(tibble), tibble, sep = " = '", collapse = "' AND ")
  sql <- str_interp("
    SELECT * FROM ${table}
    WHERE ${criteria}'
  ")

  # Send query
  result <- dbSendQuery(connection, sql)
  data <- dbFetch(result)
  dbClearResult(result)

  # Check length of match
  if (nrow(data) > 1) {
    stop("Match not unique!")
  } else if (nrow(data) == 1) {
    return(data)
  } else {
    return(NULL)
  }
}

insertPaper <- function(citationKey = NULL) {
  if (is.null(citationKey)) citationKey <- readline("Citation Key: ")
  title <- readline("Paper Title: ")
  year <- readline("Year: ")
  yournal <- readline("Published in (yournal): ")

  # Insert paper
  tibble(citationKey, title, yournal, year) %>%
    insertData("paper")

  # Insert authors
  authorNumber <- 1
  addAuthor <- 1
  while (addAuthor == 1) {
    firstName <- readline(sprintf("First name author %d: ", authorNumber))
    lastName <- readline(sprintf("Last name author %d: ", authorNumber))

    # Check Author
    key <- tibble(firstName, lastName)
    if (!matchKey(key, "author")) {
      insertData(key, "author")
    }

    # Insert paper_has_author
    tibble(citationKey, firstName, lastName, number = authorNumber) %>%
      insertData("paper_has_author")

    # Promt user to add more authors
    authorNumber <- authorNumber + 1
    addAuthor <- menu(c("Yes", "No"), title = sprintf("Add author number %d?", authorNumber))
  }
}

insertHost <- function(hostGenus = NULL, hostSpecies = NULL) {
  if (is.null(hostGenus)) hostGenus <- readline("Host Genus: ")
  if (is.null(hostSpecies)) hostSpecies <- readline("Host Species: ")

  tibble(species = hostSpecies, genus = hostGenus) %>%
    insertData("host")
}

insertStrain <- function(name = NULL, genus = NULL, species = NULL, subspecies =NULL,
                         hostGenus = NULL, hostSpecies = NULL, addAlias = NULL
  ) {
  if (is.null(name)) name <- readline("Strain Name: ")
  while (str_detect(name, " ")) {
    name <- readline("No spaces permitted! Strain Name: ")
  }
  if (is.null(genus)) genus <- readline("Strain Genus: ")
  if (is.null(species)) species <- readline("Strain Species: ")
  if (is.null(subspecies)) subspecies <- readline("Strain Subspecies: ")
  if (is.null(hostGenus)) hostGenus <- readline("Isolation Host Genus: ")
  if (is.null(hostSpecies)) hostSpecies <- readline("Isolation Host Species: ")

  # Check if host is present
  key <- tibble(genus = hostGenus, species = hostSpecies)
  if (!matchKey(key, "host")) {
    insertHost(hostGenus = hostGenus, hostSpecies = hostSpecies)
  }

  # Add strain to database
  tibble(
    name = name,
    genus = genus,
    species = species,
    subspecies = subspecies,
    hostGenus = hostGenus,
    hostSpecies = hostSpecies
  ) %>%
    insertData("strain")

  # Promt user to add alias
  if (is.null(addAlias)) addAlias <- menu(c("Yes", "No"), title = sprintf("Add alias for '%s'?", name))
  while (addAlias == 1) {
    insertAlias(name)
    addAlias <- menu(c("Yes", "No"), title = "Add another alias?")
  }
}

insertAlias <- function(name = NULL, strainAlias = NULL) {
  if (is.null(name)) name <- readline("Strain name: ")
  while (str_detect(name, " ")) {
    name <- readline("No spaces permitted! Strain Name: ")
  }

  if (is.null(strainAlias)) strainAlias <- readline(sprintf("Input alias for '%s': ", name))
  while (str_detect(strainAlias, " ")) {
    strainAlias <- readline("No spaces permitted! Input alias: ")
  }

  # Alias cannot be an already registed strain name
  key <- tibble(name = strainAlias)
  if (matchKey(key, "strain")) {
    stop(sprintf("Alias '%s' already registed as strain name", strainAlias))

  } else if (name == strainAlias) {
    warning(sprintf("Name and alias are equal for '%s'", name))

  } else {
    tibble(name = name, alias = strainAlias) %>%
      insertData("alias")
  }
}

insertTest <- function(
  strainName = NULL, citationKey = NULL, hostGenus = NULL, hostSpecies = NULL,
  method = NULL, pathogenicity = NULL, symptoms = NULL
) {
  # Strain
  if (is.null(strainName)) strainName <- readline("Strain Name: ")
  while (str_detect(strainName, "\\s")) {
    strainName <- readline("No spaces permitted! Strain Name: ")
  }

  key <- tibble(name = strainName)
  if (!matchKey(key, "strain")) {
    insertStrain(strainName)
  }

  # Host
  if (is.null(hostGenus)) hostGenus <- readline("Test Host Genus: ")
  if (is.null(hostSpecies)) hostSpecies <- readline("Test Host Species: ")

  key <- tibble(genus = hostGenus, species = hostSpecies)
  if (!matchKey(key, "host")) {
    insertHost(hostGenus = hostGenus, hostSpecies = hostSpecies)
  }

  # Citation
  if (is.null(citationKey)) citationKey <- readline("Citation Key: ")

  key <- tibble(citationKey)
  if (!matchKey(key, "paper")) {
    insertPaper(citationKey)
  }

  # Attributes
  if (is.null(method)) {
    idx <- menu(inoculationOptions, title = "Select inoculation method")
    method <- inoculationOptions[idx]
  }

  if (is.null(pathogenicity)) {
    idx <- menu(pathogenicityOptions, title = "Select resulting pathogenicity")
    pathogenicity <- pathogenicityOptions[idx]
  }

  if (is.null(symptoms)) {
    idx <- menu(symptomsOptions, title = "Select symptom severity")
    symptoms <- symptomsOptions[idx]
  }

  tibble(strainName, citationKey, hostGenus, hostSpecies, method, pathogenicity, symptoms) %>%
    insertData("test")
}

loopFunction <- function(myFunction) {
  continue <- 1
  while (continue == 1) {
    myFunction()

    continue <- menu(c("Yes", "No"), title = "Do you want to input another test?")
  }
}

parseExcelPaper <- function(path, sheetName, verbose = F) {
  data <- xlsx::read.xlsx2(path, sheetName, header = T, as.data.frame = F)

  # Loop over all entries
  for (i in 1:length(data[[1]])) {
    citationKey <- data[[1]][i]
    title <- data[[2]][i]
    year <- data[[3]][i]
    yournal <- data[[4]][i]
    authors <- data[[5]][i] %>% str_split(" and ") %>% .[[1]]

    # Check if paper is present
    key <- tibble(citationKey)
    if (!matchKey(key, "paper")) {
      # Insert paper
      tibble(citationKey, title, yournal, year) %>%
        insertData("paper")

      # Insert authors
      for (j in 1:length(authors)) {
        lastFirstName <- authors[j] %>% str_split(", ") %>% .[[1]]
        lastName <- lastFirstName[1]
        firstName <- lastFirstName[2]

        # Check Author
        key <- tibble(firstName, lastName)
        if (!matchKey(key, "author")) {
          insertData(key, "author")
        }

        # Insert paper_has_author
        tibble(citationKey, firstName, lastName, number = j) %>%
          insertData("paper_has_author")
      }

      if (verbose) message(sprintf("Sucesfully added '%s'", citationKey))

    } else {
      warning(sprintf("Paper '%s' already present", citationKey))
    }
  }
}

parseExcelTest <- function(path, sheetName, verbose = F) {
  data <- xlsx::read.xlsx2(path, sheetName = sheetName, header = T, as.data.frame = F)

  for (i in 1:length(data[[1]])) {
    name <- data[[1]][i]
    genus <- data[[2]][i]
    species <- data[[3]][i]
    subspecies <- data[[4]][i]
    isolationGenus <- data[[5]][i]
    isolationSpecies <- data[[6]][i]
    citationKey <- data[[7]][i]
    hostGenus <- data[[8]][i]
    hostSpecies <- data[[9]][i]
    method <- data[[10]][i]
    pathogenicity <- data[[11]][i]
    symptoms <- data[[12]][i]
    alias <- data[[13]][i] %>% str_remove_all(" ") %>% str_split(",") %>% .[[1]]

    # Check if name is registed as alias
    key <- tibble(alias = name)
    query <- fetchData(key, "alias")
    if (!is.null(query)) {
      message(sprintf("Strain '%s' is already registed as an alias of '%s', switching names", name, query$name))
      # Add name to alias
      alias <- c(alias, name)
      # Use registered name
      name <- query$name
      # Remove new name if it is in alias
      alias <- alias[alias != name]
    }

    # Check if alias is registered as name
    for (strainAlias in alias) {
      key <- tibble(name = strainAlias)
      if (matchKey(key, "strain")) {
        message(sprintf("Strain '%s' is already registed under '%s', switching names", name, strainAlias))
        # switch name and alias
        alias <- c(alias, name)
        name <- strainAlias
        alias <- alias[alias != name]
      }
    }

    # Add strain
    key <- tibble(name)
    if (!matchKey(key, "strain")) {
      if (verbose) message(sprintf("Adding strain '%s'", name))
      insertStrain(name, genus, species, subspecies, isolationGenus, isolationSpecies, addAlias = 2)
    }

    # Add aliases
    for (strainAlias in alias) {
      # Skip empty strings
      if (strainAlias == "") next()

      # query equals alias if it is not present
      key <- tibble(alias = strainAlias)
      query <- fetchData(key, "alias")
      if (is.null(query)) {
        insertAlias(name, strainAlias)

      } else if (name != query$name) {
        # Check if alias is not registered for a differnt strain
        stop(sprintf("'%s' given as alias of '%s', but is already registered for '%s'!", strainAlias, name, query$name))
      }
    }

    # Add test
    key <- tibble(strainName = name, citationKey, hostGenus, hostSpecies)
    if (!matchKey(key, "test")) {
      if (verbose) message(sprintf("Adding test for '%s' on '%s %s' by '%s'",
                    name, hostGenus, hostSpecies, citationKey))

      insertTest(name, citationKey, hostGenus, hostSpecies, method, pathogenicity, symptoms)
    } else {
      warning(sprintf("Test for '%s' on '%s %s' from '%s' already present",
                    name, hostGenus, hostSpecies, citationKey))
    }
  }
}


# Main --------------------------------------------------------------------

#loopFunction(insertTest())

parseExcelPaper("Data/pathogenicity_database.xlsx", sheetName = "paper")

parseExcelTest("Data/pathogenicity_database.xlsx", sheetName = "test")

# Close connection
dbDisconnect(connection)
