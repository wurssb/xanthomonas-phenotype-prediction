#' SPARQL: interface with sparql databases
#' 
#' This function provides a means to query sparql databases using curl.
#' Results are stored to speed up repeated queries
#' @param endpoint String, adress of the sparql endpoint
#' @param query String, sparql query
#' @param overwriteExisting Boolean, when true overwrites results from previously executed querries
#' @return data.table, result of the query
#' @export
#' @examples 
#' ENDPOINT = "http://ssb6.wurnet.nl:7202/repositories/XaXf"
#' 
#' genome_domain_abundance = NULL
#' 
#' for (sample in samples) {
#'   #  print(match(sample, samples))
#'   my_query = paste0("
#'     PREFIX gbol: <http://gbol.life/0.1/>
#'     SELECT DISTINCT ?sample (COUNT(DISTINCT ?feature) AS ?abundance)
#'     WHERE { 
#'         ?sample a gbol:Sample .
#'         VALUES ?sample { ",sample," } .
#'         ?contig gbol:sample ?sample .
#'         ?contig gbol:feature ?gene .
#'         ?gene gbol:transcript ?transcript .
#'         ?gene gbol:provenance ?prov .
#'         ?prov gbol:annotation ?annot .
#'         ?annot a <http://semantics.systemsbiology.nl/sapp/0.1/Prodigal> .
#'         ?transcript gbol:feature ?cds .
#'         ?cds gbol:protein ?protein .
#'         ?protein gbol:feature ?feature .
#'         ?feature gbol:xref ?xref .
#'         ?xref gbol:accession ?accession .
#'         ?xref gbol:db <http://gbol.life/0.1/db/pfam> .
#'     } GROUP BY ?sample")
#'   result = 
#'     
#'     genome_domain_abundance = rbind(genome_domain_abundance, SPARQL(endpoint = ENDPOINT, query = my_query))
#' }

SPARQLlinux <- function(endpoint, query, overwriteExisting = FALSE) {
  query = gsub(pattern = "'",replacement = '"', x = query)
  md5 = digest::digest(object = paste(endpoint, query))
  first = substring(md5, 1, 1)
  
  # Create dirs to store results
  dir.create("results", showWarnings = F)
  dir.create(path = paste0("./results/",first), showWarnings = F)
  dir.create("queries", showWarnings = F)
  dir.create(path = paste0("./queries/",first), showWarnings = F)
  
  filename = paste0("./results/",first,"/",md5)
  queryfile = paste0("./queries/",first,"/",md5)
  
  results = NULL
  
  if (!overwriteExisting && file.exists(filename) && file.size(filename) > 0) {
    # Load previously stored data
    results = fread(filename, sep = "\t", showProgress = T)
    
  } else {
    # Run query
    
    if (overwriteExisting && file.exists(filename) && file.size(filename) > 0) {
      # Delete old files
      file.remove(filename)
      file.remove(queryfile)
    }
    
    # File header
    fileConn<-file(queryfile)
    writeLines(c("#",endpoint,"#",query), fileConn)
    close(fileConn)
    
    # Prepeare curl command
    curl = paste0("curl -s -X POST ", endpoint," --data-urlencode 'query=", query,"' -H 'Accept:text/tab-separated-values' > ",filename)
    curl = gsub(pattern = "\n", replacement = " ",x = curl)
    curl = gsub(pattern = " +", replacement = " ",x = curl)
    
    # Execute qurey using curl and write to query file
    system(curl)
    
    if (file.size(filename) > 0)
      # Store results if succesfull
      results = fread(filename, sep = "\t")
  }
  colnames(results) = gsub(pattern = "\\?", replacement = "", x = colnames(results))
  
  # Remove empty columns
  results <- Filter(function(x)!all(is.na(x)), results)
  
  # Check if columns remain
  if (dim(results)[1] == 0)
    return(data.frame(Characters=character()))
  else
    return(results)
}