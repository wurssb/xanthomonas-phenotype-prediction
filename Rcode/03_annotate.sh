#!/bin/bash

if [ ! -f sapp/Conversion.jar ]; then
    echo "SAPP binaries can be downloaded from http://download.systemsbiology.nl/sapp/"
    exit 2
fi

# Convert fasta to hdt
find . -maxdepth 1 -name "*.fasta" | parallel java -jar sapp/Conversion.jar -fasta2hdt -genome -codon 11 -i {} -o {.}.hdt -id {/.}

# Call genes on hdt file
find . -maxdepth 1 -name "*.hdt" | parallel java -jar sapp/genecaller.jar -p -c 11 -s -i {} -o {.}.prodigal.hdt

# Annotate genes with PFAM and TIGRFAM domains
find . -maxdepth 1 -name "*.prodigal.hdt" | parallel java -jar sapp/InterProScan.jar -v interproscan-5.44-79.0 -a TIGRFAM,Pfam -cpu 1 -i {} -o {.}.annotated.hdt

# Convert hdt to ttl format
find . -maxdepth 1 -name "*.annotated.hdt" | parallel java -jar sapp/Conversion.jar -hdt2rdf -i {} -o {.}.ttl
