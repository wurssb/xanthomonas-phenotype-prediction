# Setup -------------------------------------------------------------------

# Renv uses the "renv.lock" file to install identical package versions
install <- F
if (install) renv::restore()

# Load packages
library(xtable)
library(rsample)
library(pls)
library(caret)
library(e1071)
library(MLmetrics)
library(glmnet)
library(randomForest)
library(rpart)
library(doParallel)
library(tidyverse)

# Load custom functions
library(MyFunctions)
#devtools::load_all("MyFunctions/")

# Clear environment
rm(list=ls())
gc()

# Functions
repeated <- function(vector) {
  difference <- vector %>%
    as.factor() %>%
    as.numeric() %>%
    diff()

  return(c(F, difference == 0))
}

# Register cores
registerDoParallel(detectCores()-2)

# Create folder to store figures and tables
dir.create("Figures", showWarnings = F)
dir.create("Tables", showWarnings = F)

# Load data, remove all non Xanthomonas strains (previously used as outgroup)
genomeInformation <- read_csv("Data/genome_information.csv") %>%
  filter(genus == "Xanthomonas") %>%
  mutate(species = paste("X.", species)) %>%
  # Recode pathogenicity for ease of use
  mutate(pathogenicity = fct_recode(pathogenicity, nonpathogenic = "non-pathogenic"))

# Add pathogenicity to domain matrix as dependant variable
domainMatrix <- read_csv("Data/domain_matrix.csv") %>%
  filter(gca %in% genomeInformation$gca) %>%
  column_to_rownames("gca") %>%
  select(which(colSums(.) > 0)) %>%
  rownames_to_column("gca") %>%
  inner_join(select(genomeInformation, gca, pathogenicity), by = "gca") %>%
  column_to_rownames("gca")

domainDescriptions <- read_csv("Data/domain_descriptions.csv") %>%
  filter(accession %in% colnames(domainMatrix))

# Theming
theme_set(theme_bw())

# Global color schemes
labelCount <- genomeInformation$species %>% unique() %>% length()
specieShape <- rep(21:25, length.out = labelCount)
pathoShape <- c(16, 0)
predColor <- rev(RColorBrewer::brewer.pal(11, "Spectral"))
obsShape <- c(21, 22)

# Seed for reproducability
mySeed <- 1234


# Filter Matrix -----------------------------------------------------------

# Remove domains only present or absent in 2.5% of genomes
variableMatrix <- domainMatrix %>% select(-nearZeroVar(., freqCut = 97.5/2.5))

# Compute correlation between domains
corMat <- variableMatrix %>% select(-pathogenicity) %>% cor()

# Remove the domain with the highest mean absolute correlation from domains
# with a pair-wise correlation of 0.8 or higher
filteredMatrix  <- variableMatrix %>% select(-findCorrelation(corMat, cutoff = 0.8))


# Enrichment --------------------------------------------------------------

# Enrichment test function
testEnrichment <- function(pathogenic, totalPathogenic, nonpathogenic, totalNonpathogenic) {
  # Loop over the pathogenic and nonpathogenic vectors simultaniously
  map2_dbl(pathogenic, nonpathogenic, function(pathogenic, nonpathogenic) {
    # Contruct contigency table
    table <- data.frame(
      row.names = c("domain_present", "domain_absent"),
      pathogenic = c(pathogenic, totalPathogenic - pathogenic),
      nonpathogenic = c(nonpathogenic, totalNonpathogenic - nonpathogenic)
    )
    # Two-side fisher exact test
    fisher.test(table)$p.value
  }) %>% p.adjust("BH") # Benjamini-Hochberg correction
}

# Count totals
totalPathogenic <- sum(domainMatrix$pathogenicity == "pathogenic")
totalNonpathogenic <- sum(domainMatrix$pathogenicity == "nonpathogenic")

# Calculate enrichment
domainEnrichment <- domainMatrix %>%
  group_by(pathogenicity) %>%
  summarise_all(sum) %>%
  pivot_longer(-pathogenicity, names_to = "accession") %>%
  pivot_wider(names_from = pathogenicity, values_from = value) %>%
  mutate(pval = testEnrichment(pathogenic,  totalPathogenic, nonpathogenic, totalNonpathogenic)) %>%
  left_join(domainDescriptions, by = "accession") %>%
  mutate(pathogenic = pathogenic/totalPathogenic) %>%
  mutate(nonpathogenic = nonpathogenic/totalNonpathogenic)


# Partial Least Squares ---------------------------------------------------

# Pls on filtered matrix
plsr <- caret::train(
  pathogenicity ~ .,
  method = "pls",
  data = filteredMatrix,
  scale = FALSE,
  center = TRUE,
  tuneGrid = expand.grid(ncomp = 2)
)

pls <- as_tibble(plsr$finalModel$scores[,1:2])

# Compute percentage explained
explained <- drop(pls::R2(plsr$finalModel, estimate = "train", intercept = FALSE)$val)[1,] * 100

p1 <- ggpubr::ggarrange(
  # Pathogenicity
  pls %>%
    bind_cols(Phenotype = filteredMatrix$pathogenicity) %>%
    mutate(Phenotype = fct_recode(Phenotype,
      `Non-pathogenic` = "nonpathogenic",
      Pathogenic = "pathogenic"
    )) %>%
    ggplot(aes(`Comp 1`, `Comp 2`, shape = Phenotype)) +
      geom_point(size = 2, color = "black") +
      scale_shape_manual(values= pathoShape) +
      xlab(sprintf("Component 1 (%.2f%%)", explained[1])) +
      ylab(sprintf("Component 2 (%.2f%%)", diff(explained)[1])) +
      theme(
        legend.margin = ggplot2::margin(t = 49, b = 49),
        legend.spacing.x = unit(.1, "points"),
        axis.title = element_text(face = "bold"),
        legend.title = element_text(face = "bold"),
        legend.direction = "vertical"
      ),

  # Species
  pls %>%
    bind_cols(Species = genomeInformation$species) %>%
    ggplot(aes(`Comp 1`, `Comp 2`, fill = Species, shape = Species)) +
      geom_point(size = 2, color = "black") +
      scale_shape_manual(values = specieShape) +
      xlab(sprintf("Component 1 (%.2f%%)", explained[1])) +
      ylab(sprintf("Component 2 (%.2f%%)", diff(explained)[1])) +
      guides(shape = guide_legend(ncol = 3)) +
      theme(
        legend.spacing.x = unit(.1, "points"),
        axis.title = element_text(face = "bold"),
        legend.title = element_text(face = "bold"),
        legend.direction = "vertical"
      ),

  labels = c("a", "b"),
  legend = "bottom"
)
print(p1)


# Model Building Setup ----------------------------------------------------

# Inner-fold training regime
trControl <- caret::trainControl(
  method = "repeatedcv",
  number = 10,
  repeats = 4,
  classProbs = TRUE
)

# Nested CV regime
set.seed(mySeed)
myResamples <- rsample::nested_cv(
  filteredMatrix,
  outside = rsample::vfold_cv(v = 5, repeats = 20, strata = "pathogenicity"),
  inside = rsample::vfold_cv(v = trControl$number, repeats = trControl$repeats)
)

# CART --------------------------------------------------------------------

# Again we use identical folds
tree <- MyFunctions::ResampleModel(
  pathogenicity ~ .,
  myResamples,
  method = "rpart",
  control = trControl,
  metric = "Accuracy",
  seed = mySeed,
  varImp = T,
  tuneLength = 9
)

#save(tree, file = "cart.Rdata")


# Lasso -------------------------------------------------------------------

# Grid, alpha = 1 == lasso
grid <- expand.grid(alpha = 1, lambda = 10^seq(-4, 0, length = 20))

# Use the identical folds
lasso <- MyFunctions::ResampleModel(
  pathogenicity ~ .,
  myResamples,
  method = "glmnet",
  control = trControl,
  metric = "Accuracy",
  seed = mySeed,
  varImp = T,
  tuneGrid = grid,
  standardize = F
)

#save(lasso, file = "lasso.Rdata")

# Random Forest -----------------------------------------------------------


# Train RandomForest with the objective of maximizing the F1-statistic
rf <- MyFunctions::ResampleModel(
  pathogenicity ~ .,
  myResamples,
  method = "rf",
  control = trControl,
  metric = "Accuracy",
  seed = mySeed,
  varImp = T,
  tuneLength = 9,
  ntree = 1000
)

#save(rf, file = "random_forest.Rdata")


# Model Statistics --------------------------------------------------------

# Function to rename modeling methods
renameMethods <- function(methods) {
  methods %>%
    fct_recode(
      `Random Forest` = "rf",
      Lasso = "glmnet",
      CART = "rpart"
    ) %>%
    fct_relevel(sort)
}

# Collect resample statistics
resampleStats <- map_df(list(tree, lasso, rf), MyFunctions::CollectResamples) %>%
  mutate(method = renameMethods(method))

# PR-curve
# Recall = correctly predicted pathogenic / observed pathogens, i.e. fraction of predicted pathogens that is correct
# Precision = correctly predicted pathogenic / predicted pathogens, i.e. fraction of observed pathogens correctly predicted
prCurve <- map_df(list(tree, lasso, rf), function(model) {
  map_df(model, ~(rownames_to_column(.$pred, "gca"))) %>%
    group_by(gca) %>%
    summarize(pathogenic = median(pathogenic), obs = as.numeric(obs) - 1, .groups = "drop") %>%
    {PRROC::pr.curve(scores.class0 = .$pathogenic, weights.class0 = .$obs, curve = T)} %>%
    .$curve %>%
    as_tibble(.name_repair = "minimal") %>%
    select(Recall = 1, Precision = 2, Treshold = 3) %>%
    mutate(method = model[[1]]$method)
}) %>%
  mutate(method = renameMethods(method))

# Plot
p2 <- ggpubr::ggarrange(
  # Boxplot
  resampleStats %>%
    select(
      method,
      `Training-set` = `Train Accuracy`,
      `Test-set` = `Test Accuracy`
    ) %>%
    pivot_longer(-method) %>%
    mutate(name = name %>% factor() %>% fct_rev()) %>%
    ggplot(aes(name, value, fill = method)) +
      geom_boxplot(width = .5, position = position_dodge(.7)) +
      scale_y_continuous(limits = c(.5, 1), breaks = seq(.5, 1, .1)) +
      scale_fill_brewer(palette = "Set1", name = "Method") +
      xlab("Set") +
      ylab("Accuracy") +
      theme(
        axis.title = element_text(face = "bold"),
        legend.title = element_text(face = "bold"),
      ),

  # PR Curve,
  prCurve %>%
    arrange(-Treshold) %>%
    ggplot(aes(Recall, Precision, color = method)) +
      geom_line() +
      geom_abline(intercept = mean(filteredMatrix$pathogenicity == "pathogenic"), slope = 0) +
      scale_y_continuous(limits = c(.5, 1), breaks = seq(.5, 1, .1)) +
      scale_x_continuous(limits = c(0, 1.05), expand = c(0, 0)) +
      scale_color_brewer(palette = "Set1", name = "Method") +
      theme(axis.title = element_text(face = "bold")),

  labels = c("c", "d"),
  common.legend = T,
  legend = "bottom"
)
print(p2)

# Combine plot with pls overview
ggpubr::ggarrange(p1, p2, nrow = 2, heights = c(1, .7))
ggsave("Figures/PLS_model_stats_combined.pdf", width = 9, height = 10)

# Precision and Recall
classStats <- map_df(list(tree, lasso, rf), function(model) {
  map_df(model, function(fold) {
    fold$pred %>%
      select(obs, pred, nonpathogenic, pathogenic) %>%
      mutate_at(vars(obs, pred), ~(factor(., levels = c("pathogenic", "nonpathogenic")))) %>%
      caret::twoClassSummary(lev = c("pathogenic", "nonpathogenic")) %>%
      {tibble(
        method = fold$method,
        `Test Sensitivity` = .["Sens"],
        `Test Specificity` = .["Spec"],
        `Test ROC` = .["ROC"]
      )}
  })
}) %>%
  mutate(method = renameMethods(method))

# Plot
classStats %>%
  select(method, `Test Sensitivity`, `Test Specificity`) %>%
  pivot_longer(-method) %>%
  mutate(group = paste(method, name)) %>%
  ggplot(aes(name, value, group = group)) +
    geom_violin(aes(color = method), position = position_dodge(1)) +
    geom_boxplot(aes(fill = method), width=0.1, position=position_dodge(1)) +
    scale_fill_brewer(palette = "Set1") +
    scale_color_brewer(palette = "Set1") +
    xlab("Metric") +
    ylab("Value") +
    theme(
      axis.title = element_text(face = "bold"),
      legend.title = element_text(face = "bold")
    )
ggsave("Figures/test_PR_violin.pdf", width = 4.5, height = 4)


# PLS Visualization -------------------------------------------------------

# Medain probability of pathogenicity mapped onto PLS
ggpubr::ggarrange(
  # Tree
  map_df(tree, ~(rownames_to_column(.$pred, "gca"))) %>%
    group_by(gca) %>%
    summarize(pathogenic = median(pathogenic)) %>%
    {inner_join(genomeInformation, ., by = "gca")} %>%
    rename(Label = pathogenicity) %>%
    mutate(Label = fct_recode(Label,
      `Non-pathogenic` = "nonpathogenic",
      Pathogenic = "pathogenic"
    )) %>%
    bind_cols(pls) %>%
    ggplot(aes(`Comp 1`, `Comp 2`, fill = pathogenic, shape = Label)) +
      geom_point(size = 2, color = "black") +
      scale_shape_manual(values = obsShape) +
      scale_fill_gradientn(
        colours = predColor,
        limits = c(0,1),
        breaks = c(0, .5, 1),
        name = NULL
      ) +
      guides(shape = guide_legend(ncol = 1)) +
      ggtitle("CART") +
      xlab(sprintf("PLS Component 1 (%.2f%%)", explained[1])) +
      ylab(sprintf("PLS Component 2 (%.2f%%)", explained[2] - explained[1])) +
      theme(
        axis.title = element_text(size = 12, face = "bold"),
        legend.title = element_text(size = 12, face = "bold"),
        legend.text = element_text(size = 12)
      ),

  # Lasso
  map_df(lasso, ~(rownames_to_column(.$pred, "gca"))) %>%
    group_by(gca) %>%
    summarize(pathogenic = median(pathogenic)) %>%
    {inner_join(genomeInformation, ., by = "gca")} %>%
    rename(Label = pathogenicity) %>%
    bind_cols(pls) %>%
    ggplot(aes(`Comp 1`, `Comp 2`, fill = pathogenic, shape = Label)) +
      geom_point(size = 2, color = "black") +
      scale_shape_manual(values = obsShape) +
      scale_fill_gradientn(colours = predColor, limits = c(0,1)) +
      ggtitle("Lasso") +
      xlab(sprintf("PLS Component 1 (%.2f%%)", explained[1])) +
      ylab(NULL) +
      theme(axis.title = element_text(size = 12, face = "bold")),

  # Random forest
  map_df(rf, ~(rownames_to_column(.$pred, "gca"))) %>%
    group_by(gca) %>%
    summarize(pathogenic = median(pathogenic)) %>%
    {inner_join(genomeInformation, ., by = "gca")} %>%
    rename(Label = pathogenicity) %>%
    bind_cols(pls) %>%
    ggplot(aes(`Comp 1`, `Comp 2`, fill = pathogenic, shape = Label)) +
      geom_point(size = 2, color = "black") +
      scale_shape_manual(values = obsShape) +
      scale_fill_gradientn(colours = predColor, limits = c(0,1)) +
      ggtitle("Random Forest") +
      xlab(sprintf("PLS Component 1 (%.2f%%)", explained[1])) +
      ylab(NULL) +
      theme(axis.title = element_text(size = 12, face = "bold")),

  ncol = 3,
  widths = c(1.05, 1, 1),
  labels = c("a", "b", "c"),
  common.legend = T,
  legend = "bottom"
)
ggsave("Figures/predictions.pdf", width = 11, height = 4)


# Domains Important For Predictions ---------------------------------------

# Collect the variable importances from all models
varImps <- map_df(list(tree, lasso, rf), function(model) {
  map_df(model, function(fold) {
    cbind(
      method = fold$method,
      rownames_to_column(fold$varImp, "accession")
    )
  })
}) %>%
  group_by(method, accession) %>%
  summarise(Overall = sum(Overall), .groups = "drop") %>%
  pivot_wider(names_from = method, values_from = Overall) %>%
  mutate_at(vars(-accession), ~(. / max(.) * 100))

# Select the 10 most important variables for each model and combine into one df
impDomains <- varImps %>%
  column_to_rownames("accession") %>%
  map_df(~slice(varImps, order(., decreasing = T)[1:10])) %>%
  filter(!duplicated(accession)) %>%
  left_join(domainEnrichment, by = "accession") %>%
  arrange(pval)

# Generate Tables ----------------------------------------------

# Xtable defaults
options(
  xtable.sanitize.colnames.function = function(x) paste0("\\textbf{", xtable::sanitize(x, type = "latex"), "}"),
  xtable.sanitize.text.function = function(x) gsub("_", "\\_", x, fixed = T),
  xtable.include.rownames = F,
  xtable.booktabs = T
)

## Most important domains for prediction
# Add the highly correlated domains and format into a table
myTable <- map_df(1:nrow(impDomains), function(i) {
  domain <- impDomains[i,][[1]]
  correlated <- colnames(corMat)[corMat[domain,] >= 0.8]
  if (length(correlated) > 1) correlated <- c(domain, correlated[correlated != domain])
  return(data.frame(domain, correlated))
}) %>%
  left_join(select(impDomains, accession, glmnet:rpart), by = c("correlated" = "accession")) %>%
  left_join(domainEnrichment, by = c("correlated" = "accession")) %>%
  transmute(
    Domain = correlated,
    #Description = str_trunc(description, width = 40),
    Description = description,
    RF = sprintf("%.1f", rf),
    Lasso = sprintf("%.1f", glmnet),
    CART = sprintf("%.1f", rpart),
    P = sprintf("%.2f", pathogenic),
    NP = sprintf("%.2f", nonpathogenic),
    Enrichment = formatC(pval, format = "e", digits = 2)
  )
print(myTable)

print(
  xtable(myTable, align = "lclrrrccr"),
  file = "Tables/combined_importance.tex"
)

## Pathogenicity distribution over the species
myTable <- genomeInformation %>%
  filter(genus == "Xanthomonas") %>%
  count(species, pathogenicity) %>%
  pivot_wider(names_from = pathogenicity, values_from = n, values_fill = list(n = 0)) %>%
  mutate(species = paste0("\\emph{", str_replace_all(species, " ", "~"), "}")) %>%
  mutate(Total = pathogenic + nonpathogenic) %>%
  arrange(-Total, -nonpathogenic, species) %>%
  column_to_rownames("species") %>%
  rbind(Total = summarise_all(., sum)) %>%
  rownames_to_column("species") %>%
  rename(Species = species, `Non-Pathogenic` = nonpathogenic, Pathogenic = pathogenic)
print(myTable)

print(
  xtable(myTable, align = "llcc|c"),
  hline.after = c(-1, 0, nrow(myTable) - 1, nrow(myTable)),
  file = "Tables/distribution.tex"
)

## Assembly statistics
myTable <- genomeInformation %>%
  select(genome_size:distinct) %>%
  {bind_rows(
    summarise_all(., min),
    summarise_all(., max),
    summarise_all(., mean)
  )} %>%
  transmute(
    Type = c("Min", "Max", "Average"),
    `Genome Size (bp)` = genome_size,
    Genes = gene_count,
    Domain = abundance,
    `Unique Domains` = distinct
  ) %>%
  pivot_longer(-Type) %>%
  pivot_wider(names_from = Type, values_from = value)
print(myTable)

print(
  xtable(myTable, align = "ll|rrr"),
  file = "Tables/assembly_stats.tex"
)


# Generate Supplementary Files --------------------------------------------

## Genome Information
genomeInformation %>%
  mutate(species = str_remove(species, "^X. ")) %>%
  as.data.frame() %>%
  xlsx::write.xlsx2(
    file = "../Supplementary_Files/genome_information.xlsx",
    sheetName = "Genome Information",
    row.names = F
  )

# Domain matrix
xlsx::write.xlsx2(
  domainMatrix,
  file = "../Supplementary_Files/domain_matrix.xlsx",
  sheetName = "Domain Presence Absence Matrix",
  row.names = T
)

# Filtered domain matrix
xlsx::write.xlsx2(
  filteredMatrix,
  file = "../Supplementary_Files/filtered_domain_matrix.xlsx",
  sheetName = "Filtered Domain Matrix",
  row.names = T
)

## Model building information
# Median model training/testing statistics
inner_join(
  resampleStats %>% group_by(method) %>% summarize_all(median),
  classStats %>% group_by(method) %>% summarize_all(median),
  by = "method"
) %>%
  as.data.frame() %>%
  xlsx::write.xlsx2(
    file = "../Supplementary_Files/model_statistics.xlsx",
    sheetName = "Median Stats",
    row.names = F
  )

# Domain Enrichement
domainEnrichment %>%
  arrange(pval) %>%
  as.data.frame() %>%
  xlsx::write.xlsx2(
    file = "../Supplementary_Files/model_statistics.xlsx",
    sheetName = "Enrichment P vs. NP",
    row.names = F,
    append = T
  )

# CART
map_df(tree, ~(rownames_to_column(.$varImp, "accession"))) %>%
  select(accession, importance = 2) %>%
  group_by(accession) %>%
  summarise(importance = sum(importance), .groups = "drop") %>%
  inner_join(domainEnrichment, by = "accession") %>%
  mutate(importance = ifelse(importance == 0, 0, importance/max(importance)) %>% round(digits = 3) * 100) %>%
  arrange(-importance) %>%
  as.data.frame() %>%
  xlsx::write.xlsx2(
    file = "../Supplementary_Files/model_statistics.xlsx",
    sheetName = "CART Importance",
    row.names = F,
    append = T
  )

# Lasso
map_df(lasso, ~(rownames_to_column(.$varImp, "accession"))) %>%
  select(accession, importance = 2) %>%
  group_by(accession) %>%
  summarise(importance = sum(importance), .groups = "drop") %>%
  inner_join(domainEnrichment, by = "accession") %>%
  mutate(importance = ifelse(importance == 0, 0, importance/max(importance)) %>% round(digits = 3) * 100) %>%
  arrange(-importance) %>%
  as.data.frame() %>%
  xlsx::write.xlsx2(
    file = "../Supplementary_Files/model_statistics.xlsx",
    sheetName = "Lasso Importance",
    row.names = F,
    append = T
  )

# Random Forest
map_df(rf, ~(rownames_to_column(.$varImp, "accession"))) %>%
  select(accession, importance = 2) %>%
  group_by(accession) %>%
  summarise(importance = sum(importance), .groups = "drop") %>%
  inner_join(domainEnrichment, by = "accession") %>%
  mutate(importance = ifelse(importance == 0, 0, importance/max(importance)) %>% round(digits = 3) * 100) %>%
  arrange(-importance) %>%
  as.data.frame() %>%
  xlsx::write.xlsx2(
    file = "../Supplementary_Files/model_statistics.xlsx",
    sheetName = "Random Forest Importance",
    row.names = F,
    append = T
  )
