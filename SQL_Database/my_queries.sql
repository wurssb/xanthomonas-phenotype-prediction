# Select tests that match the quality standards
CREATE OR REPLACE VIEW filtered AS
SELECT * FROM test
	WHERE NOT (
		pathogenicity = 'non-pathogenic' 
        AND (method = 'incision' OR hostGenus = 'Fragaria')
	);

# Select strains tested on their isolation host
CREATE OR REPLACE VIEW hostVerified AS
SELECT DISTINCT strainName FROM filtered
JOIN strain ON filtered.strainName = strain.name
	WHERE filtered.hostGenus = strain.hostGenus;

# Select non-pathogens based on selected criteria
CREATE OR REPLACE VIEW nonPathogens AS
SELECT strainName AS name, pathogenicity, GROUP_CONCAT(DISTINCT citationKey) AS source
FROM filtered
	WHERE pathogenicity = 'non-pathogenic'
		AND strainName IN (SELECT strainName FROM hostVerified)
		AND strainName NOT IN (
			SELECT strainName FROM filtered WHERE pathogenicity = 'pathogenic'
		)
GROUP BY strainName;

# Select pathogens based on selected criteria
CREATE OR REPLACE VIEW pathogens AS
SELECT strainName AS name, pathogenicity, GROUP_CONCAT(DISTINCT citationKey) AS source
FROM filtered 
	WHERE pathogenicity = 'pathogenic'
GROUP BY strainName;

# Combine information on strains & pathogenicity and add aliases
SELECT name AS strain, NULL AS alias_of, genus, species, subspecies,
	hostGenus, hostSpecies, pathogenicity, source
FROM strain JOIN nonPathogens USING(name)
UNION
SELECT alias AS strain, name AS alias_of, genus, species, subspecies,
	hostGenus, hostSpecies, pathogenicity, source
FROM alias  JOIN nonPathogens USING(name) JOIN strain USING(name)
UNION
SELECT name AS strain, NULL AS alias_of, genus, species, subspecies,
	hostGenus, hostSpecies, pathogenicity, source
FROM strain JOIN pathogens USING(name)
UNION
SELECT alias AS strain, name AS alias_of, genus, species, subspecies,
	hostGenus, hostSpecies, pathogenicity, source
FROM alias  JOIN pathogens USING(name) JOIN strain USING(name);


# Summarize test results by species
CREATE OR REPLACE VIEW rankedHosts AS
SELECT species, GROUP_CONCAT(host SEPARATOR ", ") AS host
FROM (
	SELECT 
		species, 
		CONCAT(test.hostGenus, " ", test.hostSpecies) AS host,
		COUNT(pathogenicity) AS tests
	FROM test JOIN strain ON test.strainName = strain.name
	GROUP BY species, Host
	ORDER BY tests DESC) AS tmp
GROUP BY species;

SELECT 
	CASE WHEN species = "" THEN "Xanthomonas spp." ELSE CONCAT("X.\\nbsp{}", species) END AS Species, 
	COUNT(DISTINCT strainName) AS Strains,
    SUM(CASE WHEN pathogenicity = "pathogenic" THEN 1 ELSE 0 END) AS P,
    SUM(CASE WHEN pathogenicity = "non-pathogenic" THEN 1 ELSE 0 END) AS NP,
    rankedHosts.host AS Host,
	CONCAT("\\cite{", GROUP_CONCAT(DISTINCT citationKey SEPARATOR ","), "}") AS Reference
FROM test JOIN strain ON test.strainName = strain.name JOIN rankedHosts USING(species)
GROUP BY Species, rankedHosts.host
ORDER BY Strains DESC;


# Distribution of pathogens and non-pathogens over the species
SELECT 
	CASE WHEN species = "" THEN "Xanthomonas spp." ELSE CONCAT("X.\\nbsp{}", species) END AS Species, 
	SUM(P) AS 'Pathogens',
    SUM(NP) AS 'Non-Pathogens'
FROM (
	SELECT name, 1 AS P, 0 AS NP FROM pathogens
    UNION
	SELECT name, 0 AS P, 1 AS NP FROM nonPathogens
) AS summary
JOIN strain USING(name)
GROUP BY species
ORDER BY species;
