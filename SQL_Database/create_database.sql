SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema xanthomonas
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema xanthomonas
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `xanthomonas` DEFAULT CHARACTER SET utf8 ;
USE `xanthomonas` ;

-- -----------------------------------------------------
-- Table `xanthomonas`.`host`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `xanthomonas`.`host` (
  `genus` VARCHAR(45) NOT NULL,
  `species` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`genus`, `species`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `xanthomonas`.`strain`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `xanthomonas`.`strain` (
  `name` VARCHAR(45) NOT NULL,
  `genus` VARCHAR(45) NULL,
  `species` VARCHAR(45) NULL,
  `subspecies` VARCHAR(45) NULL,
  `hostGenus` VARCHAR(45) NOT NULL,
  `hostSpecies` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`name`),
  INDEX `fk_strain_host1_idx` (`hostGenus` ASC, `hostSpecies` ASC),
  CONSTRAINT `fk_strain_host1`
    FOREIGN KEY (`hostGenus` , `hostSpecies`)
    REFERENCES `xanthomonas`.`host` (`genus` , `species`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `xanthomonas`.`alias`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `xanthomonas`.`alias` (
  `alias` VARCHAR(45) NOT NULL,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`alias`),
  CONSTRAINT `fk_ALIAS_TEST`
    FOREIGN KEY (`name`)
    REFERENCES `xanthomonas`.`strain` (`name`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `xanthomonas`.`paper`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `xanthomonas`.`paper` (
  `citationKey` VARCHAR(45) NOT NULL,
  `title` VARCHAR(450) NULL,
  `yournal` VARCHAR(100) NULL,
  `year` YEAR(4) NULL,
  PRIMARY KEY (`citationKey`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `xanthomonas`.`author`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `xanthomonas`.`author` (
  `firstName` VARCHAR(45) NOT NULL,
  `lastName` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`firstName`, `lastName`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `xanthomonas`.`paper_has_author`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `xanthomonas`.`paper_has_author` (
  `citationKey` VARCHAR(45) NOT NULL,
  `firstName` VARCHAR(45) NOT NULL,
  `lastName` VARCHAR(45) NOT NULL,
  `number` INT(8) NULL,
  PRIMARY KEY (`citationKey`, `firstName`, `lastName`),
  INDEX `fk_PAPER_has_AUTHOR_AUTHOR1_idx` (`firstName` ASC, `lastName` ASC),
  INDEX `fk_PAPER_has_AUTHOR_PAPER1_idx` (`citationKey` ASC),
  CONSTRAINT `fk_PAPER_has_AUTHOR_PAPER1`
    FOREIGN KEY (`citationKey`)
    REFERENCES `xanthomonas`.`paper` (`citationKey`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_PAPER_has_AUTHOR_AUTHOR1`
    FOREIGN KEY (`firstName` , `lastName`)
    REFERENCES `xanthomonas`.`author` (`firstName` , `lastName`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `xanthomonas`.`test`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `xanthomonas`.`test` (
  `strainName` VARCHAR(45) NOT NULL,
  `citationKey` VARCHAR(45) NOT NULL,
  `hostGenus` VARCHAR(45) NOT NULL,
  `hostSpecies` VARCHAR(45) NOT NULL,
  `method` VARCHAR(45) NULL,
  `pathogenicity` VARCHAR(45) NULL,
  `symptoms` VARCHAR(45) NULL,
  PRIMARY KEY (`strainName`, `citationKey`, `hostGenus`, `hostSpecies`),
  INDEX `fk_STRAIN_has_HOST_STRAIN1_idx` (`strainName` ASC),
  INDEX `fk_TEST_PAPER1_idx` (`citationKey` ASC),
  INDEX `fk_test_host1_idx` (`hostGenus` ASC, `hostSpecies` ASC),
  CONSTRAINT `fk_STRAIN_has_HOST_STRAIN1`
    FOREIGN KEY (`strainName`)
    REFERENCES `xanthomonas`.`strain` (`name`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_TEST_PAPER1`
    FOREIGN KEY (`citationKey`)
    REFERENCES `xanthomonas`.`paper` (`citationKey`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_test_host1`
    FOREIGN KEY (`hostGenus` , `hostSpecies`)
    REFERENCES `xanthomonas`.`host` (`genus` , `species`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
